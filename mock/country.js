
const countries = require('./data/countries.json')

module.exports = [
  {
    url: '/demo/data/countries.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  countries
      }
    }
  }
]
